extends Node2D

enum { EMPTY = -1, ACTOR, OBSTACLE }

## Start position of the player
##
## The upper left starts at (0, 0)
@export var startPt: Vector2i

## Start direction of the player.
##
## It is a clockwise order, starting at UP direction:
## 0 for "up", 1 for "right", 2 for "down" and 3 for "left"
@export var startDirection: int

func _ready():
	# Set the player at starting location
	reset()
	# Adds collision to tiles
	# Adds 'Background' and 'Objects' tileMapLayers to 'Collision' TileMapLayer
	add_coll_tiles([1,2])
	# Adds the collision to Actors
	for child in get_children():
		if( child is Pawn):
			$Collision.set_cell($Collision.local_to_map(child.position), child.type, Vector2i.ZERO)
	$Player.collision_finished.connect(reset)

## Reset the map, the player is moved to the initial position
func reset():
	# Set the player at the starting location
	#
	# At first, switch the player to the starting point on the collision map
	update_pawn_position($Player, $Collision.local_to_map($Player.position), startPt)
	# At the end, move the player
	$Player.position = $Collision.map_to_local(startPt)
	$Player.cur_direction_index = startDirection

## Used to add collision to tiles using custom data
func add_coll_tiles(layers_idx: Array[int]):
	var layers: Array[Node]
	for child in get_children():
		if( child is TileMapLayer ):
			layers.push_back(child)
	for u in layers_idx:
		var used_cells: Array[Vector2i] = layers[u].get_used_cells_by_id()
		for i in used_cells:
			# If there no cell defined in the collision layer
			var customdata_layer_src = u
			if($Collision.get_cell_source_id(i) != EMPTY):
				customdata_layer_src = 0
			# The collision custom data is copied in the collision layer
			var get_target_data: TileData = layers[customdata_layer_src].get_cell_tile_data(i)
			var cell_coll_id: int = get_target_data.get_custom_data("coll_type")
			$Collision.set_cell(i, cell_coll_id, Vector2i.ZERO)

## Used to request movement opportunity
func request_move(pawn: Node2D, direction: Vector2i) -> Vector2i:
	var cell_start: Vector2i = $Collision.local_to_map(pawn.position)
	var cell_target: Vector2i = cell_start + direction
	
	var cell_target_type: int = $Collision.get_cell_source_id(cell_target)
	match cell_target_type:
		EMPTY:
			return update_pawn_position(pawn, cell_start, cell_target)
		_:
			return Vector2i.ZERO

## Used to request dialogue opportunity
func request_diag(pawn: Node2D, direction: Vector2i):
	var cell_start: Vector2i = $Collision.local_to_map(pawn.position)
	var cell_target: Vector2i = cell_start + direction
	
	var cell_target_type: int = $Collision.get_cell_source_id(cell_target)
	match cell_target_type:
		ACTOR:
			var object_pawn: Pawn = get_cell_pawn(cell_target)
			# Checks just in case if the pawn was detected corretly
			if object_pawn:
				object_pawn.trigger_event(direction)

## Can be used to get a specific pawn in the Grid
func get_cell_pawn(coordinates: Vector2i):
	for node in get_children():
		if $Collision.local_to_map(node.position) == coordinates:
			return(node)

## Updates the pawn's collision tiles and position
func update_pawn_position(pawn: Node2D, cell_start: Vector2i, cell_target: Vector2i) -> Vector2i:
	$Collision.set_cell(cell_target, pawn.type, Vector2i.ZERO)
	$Collision.set_cell(cell_start, EMPTY, Vector2i.ZERO)
	return $Collision.map_to_local(cell_target)

## Convert position from local to map
func pos_local_to_map(pos: Vector2):
	return $Collision.local_to_map(pos)

## Convert position from local to map
func pos_map_to_local(pos: Vector2i):
	return $Collision.map_to_local(pos)

## Request a tile of the tilemap
##
## layer_name: could be "Collision", "Background" and "Objects"
## pos: local position of the requested tile, converts to map coord internally
## return: a map with "source_id" and "atlas_coords" information of the tile
func request_map_tile(layer_name: String, pos: Vector2i):
	var tileMapLayer: TileMapLayer
	if layer_name == $Collision.name:
		tileMapLayer = $Collision
	elif layer_name == $Background.name:
		tileMapLayer = $Background
	else:
		tileMapLayer = $Objects
	# the result map with "source_id" and "atlas_coords" information
	var r = {}
	var mapPos = tileMapLayer.local_to_map(pos)
	r["source_id"] = tileMapLayer.get_cell_source_id(mapPos)
	r["atlas_coords"] = tileMapLayer.get_cell_atlas_coords(mapPos)
	return r

## Remove a tile from the TileMap
##
## layer_name: could be "Collision", "Background" and "Objects"
## pos: local position of the tile to remove, converts to map coord internally
func remove_map_tile(layer_name: String, pos: Vector2i):
	var tileMapLayer: TileMapLayer
	if layer_name == $Collision.name:
		tileMapLayer = $Collision
	elif layer_name == $Background.name:
		tileMapLayer = $Background
	else:
		tileMapLayer = $Objects
	tileMapLayer.erase_cell(tileMapLayer.local_to_map(pos))

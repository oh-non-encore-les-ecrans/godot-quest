extends Node2D

## The current program where the instructions will be added
@onready var currentProgram = $MarginContainer/MainContainer/LeftContainer/Program

var memories: Array
var nb_hearts: int = 24

# Called when the node enters the scene tree for the first time.
func _ready():
	memories.append($MarginContainer/MainContainer/LeftContainer/Program)
	memories.append($MarginContainer/MainContainer/LeftContainer/Memory1)
	memories.append($MarginContainer/MainContainer/LeftContainer/Memory2)
	_on_program_focus_entered()
	# If a collision is detected -> We stop the program
	$Level01/Player.collision_detected.connect($programCore.stop)
	# If a collision is finished -> Remove a heart (life)
	$Level01/Player.collision_finished.connect(hearts_lost)
	# If the Player.energy changed -> Update the Energy bar
	$Level01/Player.energy_changed.connect(_on_player_energy_changed)
	# If an instruction has started or has finished -> Update the GUI
	$programCore.instruction_called.connect(_on_programCore_instruction_called)
	# If the instruction comes from the main program -> Player.energy - 1
	$programCore.main_program_called.connect(_update_energy)
	# Initialize the Energy bar with the Player energy value
	$Level01/EnergyBar.energy = $Level01/Player.energy
	# Update the Heart bar
	_update_hearts_bar()

func _add_instruction(inst: int, desc: String, instLoop: int, iterationLoop: int):
	_add_core_instruction(inst, instLoop, iterationLoop)
	_add_hud_instruction(inst, desc, instLoop, iterationLoop)

## Add instruction in ProgramCore
func _add_core_instruction(inst: int, instLoop: int, iterationLoop: int):
	# Add the instruction to the program
	if($programCore.addInstruction(inst, instLoop, iterationLoop) == false):
		# Show error message... TODO
		print_debug("Adding this instruction is impossible.")

## Add instruction in HUD
func _add_hud_instruction(inst: int, desc: String, instLoop: int, iterationLoop: int):
	# Add the instruction to the hid
	if(currentProgram.addInstruction(inst, desc, instLoop, iterationLoop) == false):
		# Show error message... TODO
		print_debug("No more space for new instructions.")


func _on_instructions_set_add_instruction(inst, desc, instLoop, iterationLoop):
	_add_instruction(inst, desc, instLoop, iterationLoop)


func _on_reset_button_pressed():
	# Update the ProgramCore
	$programCore.reset()
	# Update the GUI
	for mem in memories:
		mem.reset()


func _on_del_button_pressed():
	currentProgram.delInstruction()
	$programCore.delInstruction()


func _on_execute_button_pressed() -> void:
	$programCore.execute()


func _on_program_focus_entered():
	$programCore.memoriesIndex = 0
	currentProgram = memories[0]
	currentProgram.set_modulate(Color(1.0, 1.0, 1.0))
	memories[1].set_modulate(Color(0.8, 0.8, 0.8))
	memories[2].set_modulate(Color(0.8, 0.8, 0.8))

func _on_memory_1_focus_entered():
	$programCore.memoriesIndex = 1
	currentProgram = memories[1]
	currentProgram.set_modulate(Color(1.0, 1.0, 1.0))
	memories[0].set_modulate(Color(0.8, 0.8, 0.8))
	memories[2].set_modulate(Color(0.8, 0.8, 0.8))

func _on_memory_2_focus_entered():
	$programCore.memoriesIndex = 2
	currentProgram = memories[2]
	currentProgram.set_modulate(Color(1.0, 1.0, 1.0))
	memories[0].set_modulate(Color(0.8, 0.8, 0.8))
	memories[1].set_modulate(Color(0.8, 0.8, 0.8))

func _on_programCore_instruction_called(memoryIdx: int, instrIdx: int, instrSet: String, called: bool):
	# Program highlight
	for mem in memories:
		mem.set_modulate(Color(0.8, 0.8, 0.8))
	if called == true:
		memories[memoryIdx].set_modulate(Color(1, 1, 1))
	# Instruction highlight
	memories[memoryIdx].highlightInstruction(instrIdx, called)

func _on_player_energy_changed(energy: int):
	$Level01/EnergyBar.energy = energy

func hearts_lost():
	nb_hearts -= 1
	_update_hearts_bar()

func hearts_increase(nb: int):
	nb_hearts += nb
	_update_hearts_bar()

func _update_hearts_bar():
	for child in $Level01/HeartsBar.get_children():
		$Level01/HeartsBar.remove_child(child)
		child.queue_free()
	for i in 24:
		var texture = TextureRect.new()
		if(i < nb_hearts):
			texture.texture = load("./resources/objects/heart.png")
		else:
			texture.texture = load("./resources/objects/heart_empty.png")
		$Level01/HeartsBar.add_child(texture)

func _update_energy():
	# If the instruction comes from the main program -> Energy - 1
	$Level01/Player.energy -= 1

extends Node2D

var energy: int:
	set(value): 
		energy = value
		update()
	get(): return energy

var _resource: String = "res://resources/objects/energy_cell.png"

func _ready():
	update()

func _process(_delta):
	pass

func update():
	for child in get_children():
		remove_child(child)
		child.queue_free()
	for i in energy:
		var sprite = Sprite2D.new()
		sprite.position = self.position
		sprite.position.x -= i * 4
		sprite.texture = load(_resource)
		add_child(sprite)

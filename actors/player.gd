extends Pawn

signal collision_detected
signal collision_finished
signal energy_changed(new_value)

## Direction names with clockwise sorting
const DIRECTION_NAMES: Array[String] = [
	"up",
	"right",
	"down",
	"left"
]

## Map the 4 directions to their forward movment vector
const FORWARD_MOV_MAP: Dictionary = {
	"up": Vector2i.UP,
	"right": Vector2i.RIGHT,
	"down": Vector2i.DOWN,
	"left": Vector2i.LEFT
	}

## Map the 4 directions to their backward movment vector
const BACKWARD_MOV_MAP: Dictionary = {
	"up": Vector2i.DOWN,
	"right": Vector2i.LEFT,
	"down": Vector2i.UP,
	"left": Vector2i.RIGHT
	}

@export var energy: int:
	get: return energy
	set(value):
		energy = value
		energy_changed.emit(energy)
		if energy == 0:
			collision_detected.emit()

@export var speed: float = 1.0:
	get: return speed
	set(value): 
		speed = value
		if(get_node("animatedSprite2D") != null):
			$animatedSprite2D.speed_scale *= speed

# Movement Related (+ animation)
var is_moving: bool = false
var cur_direction_index: int = 0:
	get: return cur_direction_index
	set(value):
		cur_direction_index = value
		$animatedSprite2D.play("idle_" + DIRECTION_NAMES[value])

var is_talking: bool = false
var move_tween: Tween
var collision_tween: Tween

@onready var Grid: Node2D = get_parent()

func _ready():
	pass

func _process(_delta):
	pass

func _unhandled_input(event):
	
	if(event.is_action("FORWARD") == true):
		print_debug("FORWARD received!")
		move_to(FORWARD_MOV_MAP[DIRECTION_NAMES[cur_direction_index]])
	
	elif(event.is_action("BACKWARD") == true):
		print_debug("BACKWARD received!")
		move_to(BACKWARD_MOV_MAP[DIRECTION_NAMES[cur_direction_index]])
	
	elif(event.is_action("TURN_LEFT") == true):
		print_debug("TURN_LEFT received!")
		# rotation anticlockwise
		cur_direction_index = (cur_direction_index - 1) % 4
		$animatedSprite2D.play("idle_" + DIRECTION_NAMES[cur_direction_index])
	
	elif(event.is_action("TURN_RIGHT") == true):
		print_debug("TURN_RIGHT received!")
		# rotation clockwise
		cur_direction_index = (cur_direction_index + 1) % 4
		$animatedSprite2D.play("idle_" + DIRECTION_NAMES[cur_direction_index])
	
	elif(event.is_action("EAT") == true):
		print_debug("EAT received!")
		# Which element can I eat ?
		var e = Grid.request_map_tile("Objects", self.position)
		# If e is a carrot
		if e["source_id"] == 4 and e["atlas_coords"] == Vector2i(0,0):
			# Remove the carrot
			Grid.remove_map_tile("Objects", self.position)
			# Increase the energy
			energy += 6
	
	# TODO implementation of the last actions:
	elif(event.is_action("PISHING") == true):
		print_debug("PISHING received!")
	elif(event.is_action("JUMP") == true):
		print_debug("JUMP received!")

func move_to(input_direction: Vector2i):
	var target_position: Vector2i = Grid.request_move(self, input_direction)
	if target_position:
		$animatedSprite2D.play("walk_" + DIRECTION_NAMES[cur_direction_index])
		# Moves the character at the speed of the animation (which can be modified with the speed variable)
		move_tween = create_tween()
		move_tween.connect("finished", _move_tween_done)
		move_tween.tween_property(self, "position", Vector2(target_position), 1/speed)
		move_tween.tween_property($animatedSprite2D, "animation", StringName("idle_" + DIRECTION_NAMES[cur_direction_index]), 0.0)
		is_moving = true
	else:
		print_debug("Collision detected!")
		collision_detected.emit()
		_collision_anim(input_direction)
		#Grid.reset()

func _move_tween_done():
	move_tween.kill()
	is_moving = false

func _collision_tween_done():
	collision_tween.kill()
	is_moving = false
	# Workarround the collision_tween finished signal is emitted too early
	await get_tree().create_timer(3.0).timeout
	collision_finished.emit()

func _collision_anim(input_direction: Vector2i):
	# Begin to walk
	$animatedSprite2D.play("walk_" + DIRECTION_NAMES[cur_direction_index])
	# Moves the character at the speed of the animation (which can be modified with the speed variable)
	var start_position = self.position
	var local_step = Grid.pos_map_to_local(input_direction) - Grid.pos_map_to_local(Vector2i(0,0))
	var target_position = start_position + local_step * 0.3
	collision_tween = create_tween()
	collision_tween.connect("finished", _collision_tween_done)
	# Move the to 30% of a step
	collision_tween.tween_property(self, "position", Vector2(target_position), 1/speed * 0.3)
	# Switch to the IDLE frame
	collision_tween.tween_property($animatedSprite2D, "animation", StringName("idle_" + DIRECTION_NAMES[cur_direction_index]), 0.0)
	# Go back quikly to the origin possition
	collision_tween.tween_property(self, "position", Vector2(start_position), 1/speed * 0.1)
	# Wait during the period of the speed
	collision_tween.tween_property(self, "position", Vector2(start_position), 1/speed)
	# Switch to the DIED frame
	collision_tween.tween_property($animatedSprite2D, "animation", StringName("died_" + DIRECTION_NAMES[cur_direction_index]), 0.0)
	is_moving = true

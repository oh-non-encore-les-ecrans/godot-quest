## Core classes to manage program execution including extension memories and 
## every instructions
class_name ProgramCore extends Node

## Instruction set
enum InstructionSet {EMPTY, FORWARD, BACKWARD, TURN_LEFT, TURN_RIGHT, EAT, PISHING, JUMP, LOOP, MEMORY1, MEMORY2}

## Map the instructions with their action name
const ActionNames = {
		InstructionSet.EMPTY: "",
		InstructionSet.FORWARD: "FORWARD",
		InstructionSet.BACKWARD: "BACKWARD",
		InstructionSet.TURN_LEFT: "TURN_LEFT",
		InstructionSet.TURN_RIGHT: "TURN_RIGHT",
		InstructionSet.EAT: "EAT",
		InstructionSet.PISHING: "PISHING",
		InstructionSet.JUMP: "JUMP",
		InstructionSet.LOOP: "LOOP",
		InstructionSet.MEMORY1: "MEMORY1",
		InstructionSet.MEMORY2: "MEMORY2"
}

## Used to display which instruction is running
signal instruction_called(memoryIdx: int, instrIdx: int, instrSet: String, called: bool)

signal main_program_called()

## Instructions of the main program
var programMain: MemoryCore

## Size of the main program in number of instructions
@export var programSize : int:
	get: return programMain.size
	set(value): programMain.size = value
## Array of memories extensions
var memories : Array[MemoryCore] = []

## Size of each memory extension in number of instructions
@export var memoriesSize : int:
	get: return memoriesSize
	set(value):
		memoriesSize = value
		for i in memories.size():
			memories[0].size = value

## Nummber of memories extensions
@export var memoriesNb : int:
	get: 	return memories.size()
	set(value):
		memories.resize(value)
		for i in memories.size():
			if (memories[i] == null):
				memories[i] = MemoryCore.new(memoriesSize, timeDelayBetweenInstructions, self, i+1)
				add_child(memories[i])

## Index to select the current memory
##
## We have to set it before adding new instruction. By this way the new 
## instruction is added at the good place
## 0 -> the main program
## 1 -> the first memory extension (M1)
## 2 -> the second memory extension (M2)
## ...
var memoriesIndex: int:
	get:	return memoriesIndex
	set(value):
		memoriesIndex = value % (1+memoriesNb)

## Time delay between the execution of 2 instructions in ms
@export var timeDelayBetweenInstructions: int:
	get:	return timeDelayBetweenInstructions
	set(value):
		timeDelayBetweenInstructions = value
		# Propagate the value in every instructions
		for mem in memories:
			for inst in mem.programMemory:
				if inst:
					inst.timeDelayBetweenInstructions = value


## ProgramCore constructor
func _init(_programSize := 20, _memoriesNb := 2, _memoriesSize := 10, _timeDelayBetweenInstructions := 1000):
	# Instantiate the main program
	programMain = MemoryCore.new(_programSize, _timeDelayBetweenInstructions, self, 0)
	add_child(programMain)
	
	# Instantiate the extension memories
	memories.resize(_memoriesNb)
	for i in memories.size():
		memories[i] = MemoryCore.new(_memoriesSize, _timeDelayBetweenInstructions, self, i+1)
		add_child(memories[i])
	memoriesSize = _memoriesSize
	
	# initialize members
	timeDelayBetweenInstructions = _timeDelayBetweenInstructions
	memoriesIndex = 0

## Execute the program
func execute():
	# We need to launch the execution of the main program only
	programMain.execute()

## Stop the execution of the program
func stop():
	programMain.stop()
	for mem in memories:
		mem.stop()

## Reset every instructions in the main program and the memories 
func reset():
	programMain.reset()
	for i in memories.size():
		memories[i].reset()

## Add a extension memory for the program
func addMem():
	memories.append(MemoryCore.new(memoriesSize, timeDelayBetweenInstructions, self, memoriesNb+2))
	add_child(memories.back())

## Add an instruction in the current memory
func addInstruction(instruction: InstructionSet, instLoop: InstructionSet, iterationLoop: int) -> bool:
	if(memoriesIndex == 0):
		return programMain.addInstruction(instruction, instLoop, iterationLoop)
	else:
		return memories[memoriesIndex-1].addInstruction(instruction, instLoop, iterationLoop)

## Delete the last instruction of the current memory
func delInstruction() -> bool:
	if(memoriesIndex == 0):
		return programMain.delInstruction()
	else:
		return memories[memoriesIndex-1].delInstruction()

func _send_instruction_called(memoryIdx: int, instrIdx: int, instrSet: String, called: bool):
	instruction_called.emit(memoryIdx, instrIdx, instrSet, called)


## Class to manage one memory
class MemoryCore extends Node:
	## Instructions of the main program
	var programMemory: Array[InstructionCore] = []
	## index of the first empty instruction
	var index: int = 0
	## link to ProgramCore
	static var programCore: ProgramCore
	## index of this memory inside 
	var memoryIndex: int = 0
	var running: bool = false
	
	## Size of the memory in number of instructions
	var size: int:
		get: 	return programMemory.size()
		set(value):
			# Set the size of the programMemory
			programMemory.resize(value)
			# Fill the programMemory with empty instructions
			for i in programMemory.size():
				if (programMemory[i] == null):
					programMemory[i] = InstructionCore.new(memoryIndex, i, InstructionSet.EMPTY)
					add_child(programMemory[i])
				else:
					programMemory[i].instructionSet = InstructionSet.EMPTY
	
	var timeDelayBetweenInstructions: int
	
	func _init(_size : int, _timeDelay : int, core : ProgramCore, _memoryIndex: int):
		# Set the size of the programMemory
		programMemory.resize(_size)
		# initialize others members
		timeDelayBetweenInstructions = _timeDelay
		index = 0
		programCore = core
		memoryIndex = _memoryIndex

	func _ready():
		# Fill the programMemory with empty instructions
		for i in programMemory.size():
			programMemory[i] = InstructionCore.new(memoryIndex, i, InstructionSet.EMPTY)
			add_child(programMemory[i])

	func execute():
		running = true
		for i in programMemory:
			# If it is an empty instruction OR if we have stop
			if(i.isEmpty() or running == false):
				running = false
				break # Stop execution
			else:
				# If we are the main program
				if memoryIndex == 0:
					# Then the signal is emmited: one instruction of the main program is called
					programCore.main_program_called.emit()
				# Execute the instruction
				await i.execute()
		running = false
	
	func stop():
		running = false
		for i in programMemory:
			i.stop()
	
	## Reset the instructions
	func reset():
		for i in programMemory.size():
			programMemory[i].instructionSet = InstructionSet.EMPTY
		index = 0
	
	## Add an instruction in this memory
	func addInstruction(instruction: InstructionSet, instLoop: InstructionSet, iterationLoop: int) -> bool:
		if (index < programMemory.size()):
			# MEMORY 1
			if (instruction == ProgramCore.InstructionSet.MEMORY1):
				if (programCore.memoriesNb < 1):
					print_debug("Sorry, there is no memory available.")
					return false
				programMemory[index] = MemoryInstructionCore.new(programCore.memories[0], 
																memoryIndex, index, instruction)
				add_child(programMemory[index])
			# MEMORY 2
			elif (instruction == ProgramCore.InstructionSet.MEMORY2):
				if (programCore.memoriesNb < 2):
					print_debug("Sorry, there is no memory number 2.")
					return false
				programMemory[index] = MemoryInstructionCore.new(programCore.memories[1], 
																memoryIndex, index, instruction)
				add_child(programMemory[index])
			# LOOP
			elif (instruction == ProgramCore.InstructionSet.LOOP):
				var instructionCore
				if( instLoop == InstructionSet.MEMORY1):
					instructionCore = MemoryInstructionCore.new(programCore.memories[0], 
																memoryIndex, index, instLoop)
				elif( instLoop == InstructionSet.MEMORY2):
					instructionCore = MemoryInstructionCore.new(programCore.memories[1], 
																memoryIndex, index, instLoop)
				else:
					instructionCore = InstructionCore.new(memoryIndex, index, 
															instLoop, timeDelayBetweenInstructions)
				programMemory[index] = LoopInstructionCore.new(memoryIndex, index, 
																instructionCore, iterationLoop)
				add_child(programMemory[index])
				programMemory[index].add_child(instructionCore)
			# Other instructions
			else:
				programMemory[index].instructionSet = instruction
			index += 1
			return true
		else:
			print_debug("Adding instruction is not allowed. The program is full.")
			return false
	
	## Delete the last instruction in this memory
	func delInstruction() -> bool:
		if(index > 0):
			index -= 1
			programMemory[index].instructionSet = InstructionSet.EMPTY
			return true
		else:
			return false

## Definition of an instruction that could be executed
class InstructionCore extends Node:
	
	var instructionSet: InstructionSet = InstructionSet.EMPTY
	var timeDelayBetweenInstructions: int
	var memoriesIndex: int
	var instructionIndex: int
	var programCore: ProgramCore

	func _init(_memoriesIndex: int, _instructionIndex: int, 
				_instructionSet := InstructionSet.EMPTY, delay := 1000):
		instructionSet = _instructionSet
		timeDelayBetweenInstructions = delay
		memoriesIndex = _memoriesIndex
		instructionIndex = _instructionIndex

	func _ready():
		programCore = null
		if get_node("/root/Main/programCore") is ProgramCore:
			programCore = get_node("/root/Main/programCore")

	func execute():
		# Emit an action for each instruction in order to simulate an input
		var action = InputEventAction.new()
		action.action = ActionNames[instructionSet]
		action.pressed = true
		Input.parse_input_event(action)
		programCore._send_instruction_called(memoriesIndex,
											instructionIndex,
											ActionNames[instructionSet],
											true)
		# wait a delay before next instruction
		await get_tree().create_timer(timeDelayBetweenInstructions / 1000.0).timeout
		programCore._send_instruction_called(memoriesIndex,
											instructionIndex,
											ActionNames[instructionSet],
											false)
	
	func isEmpty() -> bool:
		return (instructionSet == InstructionSet.EMPTY)
	
	func stop():
		pass


## MemoryInstruction is a specific instruction to launch the execution of a 
## memory. 
##
## It could be called 'M1' or 'M2', ... 
class MemoryInstructionCore extends InstructionCore:
	var memory: MemoryCore

	func _init(_mem: MemoryCore, _memoriesIndex: int, _instructionIndex: int, 
				_instructionSet: int):
		memory = _mem
		super(_memoriesIndex, _instructionIndex, _instructionSet, 0)
	
	func execute():
		var action = InputEventAction.new()
		action.action = ActionNames[instructionSet]
		action.pressed = true
		Input.parse_input_event(action)
		programCore._send_instruction_called(memoriesIndex,
											instructionIndex,
											ActionNames[instructionSet],
											true)
		await memory.execute()
		programCore._send_instruction_called(memoriesIndex,
											instructionIndex,
											ActionNames[instructionSet],
											false)


## LoopInstruction is a specific instruction to be executed N times
class LoopInstructionCore extends InstructionCore:
	var instruction: InstructionCore
	var nbIteration: int
	var running: bool = false

	func _init(_memoriesIndex: int, _instructionIndex: int, 
				_i: InstructionCore, _nbIt: int):
		instruction = _i
		nbIteration = _nbIt
		super(_memoriesIndex, _instructionIndex, InstructionSet.LOOP, 0)

	func execute():
		var action = InputEventAction.new()
		action.action = ActionNames[InstructionSet.LOOP]
		action.pressed = true
		Input.parse_input_event(action)
		programCore._send_instruction_called(memoriesIndex,
											instructionIndex,
											ActionNames[instructionSet],
											true)
		running = true
		for i in range(nbIteration):
			if running == true:
				await instruction.execute()
		programCore._send_instruction_called(memoriesIndex,
											instructionIndex,
											ActionNames[instructionSet],
											false)
		running = false
	
	func stop():
		running = false

extends GridContainer

const instructionsImagesMap = {
	ProgramCore.InstructionSet.EMPTY: "res://resources/empty_instr.png",
	ProgramCore.InstructionSet.FORWARD: "res://resources/forward.png",
	ProgramCore.InstructionSet.BACKWARD: "res://resources/backward.png",
	ProgramCore.InstructionSet.TURN_LEFT: "res://resources/turnleft.png",
	ProgramCore.InstructionSet.TURN_RIGHT: "res://resources/turnright.png",
	ProgramCore.InstructionSet.EAT: "res://resources/eat.png",
	ProgramCore.InstructionSet.PISHING: "res://resources/pishing.png",
	ProgramCore.InstructionSet.JUMP: "res://resources/jump.png",
	ProgramCore.InstructionSet.LOOP: "res://resources/loop.png",
	ProgramCore.InstructionSet.MEMORY1: "res://resources/m1.png",
	ProgramCore.InstructionSet.MEMORY2: "res://resources/m2.png"
}
## index of the first empty instruction
var index: int = 0

@export var nbInstructions: int = 20:
	get: return nbInstructions
	set(value):
		var children: Array[Node] = get_children()
		if value > children.size():
			print_debug("The instruction nb is greater than the nb of children.")
		if value > 20:
			print_debug("The instruction nb is greater than 20.")
		nbInstructions = value
		for i in range(0, value):
			children[i].visible = true
		for i in range(value, 20):
			children[i].visible = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

## Reset the instructions
func reset():
	var children = get_children()
	for i in children.size():
		children[i].texture = load("res://resources/empty_instr.png")
	index = 0

## Add an instruction in this memory
func addInstruction(instruction: ProgramCore.InstructionSet, desc :String,
					 instLoop: int, iterationLoop: int) -> bool:
	# If there is free space
	if(index < nbInstructions):
		# For every instructions execpt the LOOP
		if( instruction != ProgramCore.InstructionSet.LOOP):
			get_children()[index].texture = load(instructionsImagesMap[instruction])
		# If it is a LOOP instruction
		else:
			# Build a dynamic tilled instruction image
			var loopIcon = Image.create(32, 32, false, Image.FORMAT_RGB8)
			var loopTexture = load(instructionsImagesMap[ProgramCore.InstructionSet.LOOP])
			var instTexture = load(instructionsImagesMap[instLoop])
			var iterationTexture = load("res://resources/"+str(iterationLoop)+".png")
			loopIcon.fill(Color8(255, 255, 255))
			# Stretch the height x 2
			for i in range(16):
				loopIcon.blit_rect(loopTexture.get_image(), Rect2i(0, i, 16, i), 
								Vector2i(0, i*2))
				loopIcon.blit_rect(loopTexture.get_image(), Rect2i(0, i, 16, i), 
								Vector2i(0, i*2+1))
			loopIcon.blit_rect(instTexture.get_image(), Rect2i(0, 0, 16, 16), 
							Vector2i(16, 0))
			loopIcon.blit_rect(iterationTexture.get_image(), Rect2i(0, 0, 16, 16), 
							Vector2i(16, 16))
			get_children()[index].texture = ImageTexture.create_from_image(loopIcon)
			var font = load("res://resources/PublicPixel.otf")
		get_children()[index].tooltip_text = desc
		index += 1
		return true
	else:
		return false

## Delete the last instruction in this memory
func delInstruction() -> bool:
	if(index > 0):
		index -= 1
		get_children()[index].texture = load("res://resources/empty_instr.png")
		return true
	else:
		return false

## Highlight an instruction
func highlightInstruction(instrIndex: int, enable: bool):
	var children = get_children()
	if enable == true:
		if(instrIndex != -1):
			children[instrIndex].set_modulate(Color.from_hsv(0.1, 0.3, 1.0, 1.0))
		else:
			for i in children.size():
				children[i].set_modulate(Color.from_hsv(0.1, 0.3, 1.0, 1.0))
	else:
		if(instrIndex != -1):
			children[instrIndex].set_modulate(Color8(255, 255, 255))
		else:
			for i in children.size():
				children[i].set_modulate(Color8(255, 255, 255))

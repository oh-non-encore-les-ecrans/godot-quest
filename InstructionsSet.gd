class_name InstructionsSet
extends GridContainer

signal addInstruction(inst: int, desc: String, loopInst: int, loopIt: int)

@export var instructionsVisible = {
	ProgramCore.ActionNames[ProgramCore.InstructionSet.FORWARD]: true,
	ProgramCore.ActionNames[ProgramCore.InstructionSet.BACKWARD]: true,
	ProgramCore.ActionNames[ProgramCore.InstructionSet.TURN_LEFT]: true,
	ProgramCore.ActionNames[ProgramCore.InstructionSet.TURN_RIGHT]: true,
	ProgramCore.ActionNames[ProgramCore.InstructionSet.EAT]: true,
	ProgramCore.ActionNames[ProgramCore.InstructionSet.PISHING]: true,
	ProgramCore.ActionNames[ProgramCore.InstructionSet.JUMP]: true,
	ProgramCore.ActionNames[ProgramCore.InstructionSet.LOOP]: true,
	ProgramCore.ActionNames[ProgramCore.InstructionSet.MEMORY1]: true,
	ProgramCore.ActionNames[ProgramCore.InstructionSet.MEMORY2]: true
}:
	set(value): 
		instructionsVisible = value
		if( $ForwardButton != null ): 
			$ForwardButton.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.FORWARD]]
		if( $BackwardButton != null ): 
			$BackwardButton.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.BACKWARD]]
		if( $TurnleftButton != null ): 
			$TurnleftButton.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.TURN_LEFT]]
		if( $TurnrightButton != null ): 
			$TurnrightButton.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.TURN_RIGHT]]
		if( $EatButton != null ): 
			$EatButton.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.EAT]]
		if( $M1Button != null ): 
			$M1Button.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.MEMORY1]]
		if( $M2Button != null ): 
			$M2Button.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.MEMORY2]]
		if( $PishingButton != null ): 
			$PishingButton.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.PISHING]]
		if( $LoopButton != null ): 
			$LoopButton.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.LOOP]]
		if( $JumpButton != null ): 
			$JumpButton.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.JUMP]]

# Called when the node enters the scene tree for the first time.
func _ready():
	# workarrond because the setter is called before ready() 
	instructionsVisible = instructionsVisible
	# connect the close event of the loop window
	$LoopWindow.close_requested.connect(_on_loop_window_close_requested)


func _on_forward_button_pressed():
	addInstruction.emit(ProgramCore.InstructionSet.FORWARD, $ForwardButton.tooltip_text, 0, 0)

func _on_backward_button_pressed():
	addInstruction.emit(ProgramCore.InstructionSet.BACKWARD, $BackwardButton.tooltip_text, 0, 0)

func _on_turnleft_button_pressed():
	addInstruction.emit(ProgramCore.InstructionSet.TURN_LEFT, $TurnleftButton.tooltip_text, 0, 0)

func _on_turnright_button_pressed():
	addInstruction.emit(ProgramCore.InstructionSet.TURN_RIGHT, $TurnrightButton.tooltip_text, 0, 0)

func _on_eat_button_pressed():
	addInstruction.emit(ProgramCore.InstructionSet.EAT, $EatButton.tooltip_text, 0, 0)

func _on_m1_button_pressed():
	addInstruction.emit(ProgramCore.InstructionSet.MEMORY1, $M1Button.tooltip_text, 0, 0)

func _on_m2_button_pressed():
	addInstruction.emit(ProgramCore.InstructionSet.MEMORY2, $M2Button.tooltip_text, 0, 0)

func _on_loop_button_pressed():
	# Adjust the window to select an instruction and a number of iteration
	$LoopWindow.move_to_center()
	$LoopWindow.grab_focus()
	# Map the available instructions
	$LoopWindow/VBox/InstructionContainer/ForwardButtonL.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.FORWARD]]
	$LoopWindow/VBox/InstructionContainer/BackwardButtonL.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.BACKWARD]]
	$LoopWindow/VBox/InstructionContainer/TurnleftButtonL.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.TURN_LEFT]]
	$LoopWindow/VBox/InstructionContainer/TurnrightButtonL.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.TURN_RIGHT]]
	$LoopWindow/VBox/InstructionContainer/EatButtonL.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.EAT]]
	$LoopWindow/VBox/InstructionContainer/M1ButtonL.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.MEMORY1]]
	$LoopWindow/VBox/InstructionContainer/M2ButtonL.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.MEMORY2]]
	$LoopWindow/VBox/InstructionContainer/PishingButtonL.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.PISHING]]
	$LoopWindow/VBox/InstructionContainer/JumpButtonL.visible = instructionsVisible[ProgramCore.ActionNames[ProgramCore.InstructionSet.JUMP]]
	# Adjust the InstructionContainer width to the number of instructions
	var nbInstr: int = -1
	for i in instructionsVisible.values():
		nbInstr += int(i)
	$LoopWindow/VBox/InstructionContainer.columns = nbInstr
	_on_iteration_slider_value_changed($LoopWindow/VBox/IterationSlider.value)
	# Show the window
	$LoopWindow.show()


func _on_pishing_button_pressed():
	addInstruction.emit(ProgramCore.InstructionSet.PISHING, $PishingButton.tooltip_text, 0, 0)

func _on_jump_button_pressed():
	addInstruction.emit(ProgramCore.InstructionSet.JUMP, $JumpButton.tooltip_text, 0, 0)

func _on_loop_window_close_requested():
	$LoopWindow.hide()

func _on_iteration_slider_value_changed(value):
	$LoopWindow/VBox/HBox/ItNumberLabel.text = str(value)

func _on_ok_button_pressed():
	$LoopWindow.hide()
	var instr = ProgramCore.InstructionSet.EMPTY
	if( $LoopWindow/VBox/InstructionContainer/ForwardButtonL.button_pressed == true):
		instr = ProgramCore.InstructionSet.FORWARD
	elif( $LoopWindow/VBox/InstructionContainer/BackwardButtonL.button_pressed == true):
		instr = ProgramCore.InstructionSet.BACKWARD
	elif( $LoopWindow/VBox/InstructionContainer/TurnleftButtonL.button_pressed == true):
		instr = ProgramCore.InstructionSet.TURN_LEFT
	elif( $LoopWindow/VBox/InstructionContainer/TurnrightButtonL.button_pressed == true):
		instr = ProgramCore.InstructionSet.TURN_RIGHT
	elif( $LoopWindow/VBox/InstructionContainer/EatButtonL.button_pressed == true):
		instr = ProgramCore.InstructionSet.EAT
	elif( $LoopWindow/VBox/InstructionContainer/M1ButtonL.button_pressed == true):
		instr = ProgramCore.InstructionSet.MEMORY1
	elif( $LoopWindow/VBox/InstructionContainer/M2ButtonL.button_pressed == true):
		instr = ProgramCore.InstructionSet.MEMORY2
	elif( $LoopWindow/VBox/InstructionContainer/PishingButtonL.button_pressed == true):
		instr = ProgramCore.InstructionSet.PISHING
	elif( $LoopWindow/VBox/InstructionContainer/JumpButtonL.button_pressed == true):
		instr = ProgramCore.InstructionSet.JUMP
	var nbIt = $LoopWindow/VBox/IterationSlider.value
	# TODO generate a specific image to show the repeated instruction and the number of iteration. 
	addInstruction.emit(ProgramCore.InstructionSet.LOOP, $LoopButton.tooltip_text, instr, nbIt)
